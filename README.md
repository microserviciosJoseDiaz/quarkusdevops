### QuarkusDevOps

Ejemplo de microservicio con Quarkus

### Pre Requisitos

- Un Entorno de Desarrollo Java (Netbeans, Eclipse, etc)
- JDK 1.8+ instalado y configurado JAVA_HOME
- Apache Maven 3.5.3+

### Clonar el proyecto 

```
    git clone https://gitlab.com/microserviciosJoseDiaz/quarkusdevops.git
```


### Ejecutar la aplicacion

```
    ./mvnw compile quarkus:dev:
```

### Empaquetar 

Este comando produce 2 archivos:
 - prueba-rest-1.0-SNAPSHOT.jar   ==========> Contiene las clases y recursos
 - prueba-rest-1.0-SNAPSHOT-runner.jar ====> Es un ejecutable del proyecto no es un über-jar

```
   ./mvnw package
```

### Ejecutar la aplicacion

```
   java -jar target/prueba-rest-1.0-SNAPSHOT-runner.jar 
```



Referencias:
- https://quarkus.io/guides/getting-started-guide
- https://medium.com/@nicklonginow/continuous-delivery-pipelines-to-google-kubernetes-engine-with-gitlab-d65e04be6c0b
- https://medium.com/@vdespa/how-to-run-postman-api-tests-with-newman-in-gitlab-ci-the-proper-way-b5a9f6076d79
- https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/
- https://www.digitalocean.com/community/tech_talks/creating-ci-cd-pipeline-with-gitlab-and-digitalocean-managed-kubernetes
