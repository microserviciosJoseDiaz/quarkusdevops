package com.jose;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.PATCH;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/DevOps")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Prueba {
    
    private static final String REST_API_KEY = "X-Parse-REST-API-Key";
    private static final String KEY = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c";
    @Context 
    private HttpHeaders headers;
            
    @POST    
    public Response mensaje(MensajeEntrada mensaje ) {
        
        
        if (validarCabecera()) {
            MensajeSalida mensajeSalida = new MensajeSalida("Hello " + mensaje.getTo() + " your message will be send");
            return Response.ok(mensajeSalida).build();
        } else {
           return Response.status(Response.Status.FORBIDDEN).entity("Forbidden").build();
        }    
    }
    
    private boolean validarCabecera () {
      return !headers.getRequestHeader(REST_API_KEY).isEmpty() && 
            headers.getRequestHeader(REST_API_KEY).get(0).equals(KEY);
    }
    
    @GET
    @PUT
    @HEAD
    @DELETE
    @OPTIONS
    @PATCH
    @Produces(MediaType.TEXT_PLAIN)
    public String mensaje() {
        MensajeSalida mensaje = new MensajeSalida("Nuevo Mensaje");
        mensaje.setMessage("ERROR");
        return mensaje.getMessage();
    }
}