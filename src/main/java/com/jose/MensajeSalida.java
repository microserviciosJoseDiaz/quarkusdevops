/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jose;

import java.io.Serializable;

/**
 *
 * @author jose
 */
public class MensajeSalida implements Serializable {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MensajeSalida(String message) {
        this.message = message;
    }
}
