package com.jose;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import javax.ws.rs.core.MediaType;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class PruebaTest {

    @Test
    public void deberiaObetenerGet() {
        given()
          .when().get("/DevOps")
          .then()
             .statusCode(200)
             .body(is("ERROR"));
    }

    @Test
    public void deberiaPermitirPost() {
        given()
          .body("{\"message\": \"This is a test\", \"to\": \"Juan Perez\", \"from\": \"Rita Asturia\", \"timeToLifeSec\": \"45\"}")
          .header("X-Parse-REST-API-Key", "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c")
          .header("Content-Type", MediaType.APPLICATION_JSON)      
          .when().post("DevOps")
          .then()
              .statusCode(200)
              .body(is("{\"message\":\"Hello Juan Perez your message will be send\"}"));
}
    
     @Test
    public void deberiaRechazarPostClaveErronea() {
        given()
          .body("{\"message\": \"This is a test\", \"to\": \"Juan Perez\", \"from\": \"Rita Asturia\", \"timeToLifeSec\": \"45\"}")
          .header("X-Parse-REST-API-Key", "ClaveError")
          .header("Content-Type", MediaType.APPLICATION_JSON)      
          .when().post("DevOps")
          .then()
              .statusCode(403)
              .body(is("Forbidden"));
}
    
    @Test
    public void deberiaRechazarPost() {
        given()
          .body("{\"message\": \"This is a test\", \"to\": \"Juan Perez\", \"from\": \"Rita Asturia\", \"timeToLifeSec\": \"45\"}")         
          .header("Content-Type", MediaType.APPLICATION_JSON)      
          .when().post("DevOps")
          .then()
              .statusCode(403)
              .body(is("Forbidden"));
}

    
    @Test
    public void deberiaObetenerHead() {
        given()
          .when().head("/DevOps")
          .then()
             .statusCode(200)
             ;
    }
    
     @Test
    public void deberiaObetenerDelete() {
        given()
          .when().delete("/DevOps")
          .then()
             .statusCode(200)
             .body(is("ERROR"));
    }
   
     @Test
    public void deberiaObetenerPut() {
        given()
          .when().put("/DevOps")
          .then()
             .statusCode(200)
             .body(is("ERROR"));
    }
    
    @Test
    public void deberiaObetenerOptions() {
        given()
          .when().options("/DevOps")
          .then()
             .statusCode(200)
             .body(is("ERROR"));
    }
    
    @Test
    public void deberiaObetenerPatch() {
        given()
          .when().patch("/DevOps")
          .then()
             .statusCode(200)
             .body(is("ERROR"));
    }
    
}